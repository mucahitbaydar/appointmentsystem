## Appointment System
Simple appointment system where user can add, delete or update appointments.

## Motivation
This is an exercise project to go into learning spring boot and its features. 

## Installation
You can download the project using git
```
git clone https://mucahitbaydar@bitbucket.org/mucahitbaydar/appointmentsystem.git
```

Then you can build and create jar file using maven

```
mvn install
```

##Usage
Once you started application you can use Rest commands 
(Assuming you didn't change any configuration)

To see every appointments in system send Get request with
```
http://localhost:8080/AppointmentSystem/api/app/
```

To delete appointment with id send Delete request with
```
http://localhost:8080/AppointmentSystem/api/app/2
```