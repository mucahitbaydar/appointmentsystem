package com.baydar.as;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.baydar.as.model.Appointment;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AppointmentSystemApplicationTests {
    public static final String REST_SERVICE_URI = "http://localhost:8080/AppointmentSystem/api";
    
    /* GET */
    @SuppressWarnings("unchecked")
    private static void listAllAppointments(){
        System.out.println("Testing listAllAppointment API-----------");
         
        RestTemplate restTemplate = new RestTemplate();
        List<LinkedHashMap<String, Object>> appMap = restTemplate.getForObject(REST_SERVICE_URI+"/app/", List.class);
         
        if(appMap!=null){
            for(LinkedHashMap<String, Object> map : appMap){
                System.out.println("Appointment : id="+map.get("id")+", Name="+map.get("patient")+", Date="+map.get("date"));
            }
        }else{
            System.out.println("No appointment exist----------");
        }
    }
     
    /* GET */
    private static void getAppointment(){
        System.out.println("Testing getAppointment API----------");
        RestTemplate restTemplate = new RestTemplate();
        Appointment app = restTemplate.getForObject(REST_SERVICE_URI+"/app/1", Appointment.class);
        System.out.println(app);
    }
     
    /* POST */
    private static void createAppointment() throws ParseException {
        System.out.println("Testing create Appointment API----------");
        RestTemplate restTemplate = new RestTemplate();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String dateInString = "11-11-2011";
		Date date = sdf.parse(dateInString);
        Appointment app = new Appointment(0,"Sarah Connor",date);
        URI uri = restTemplate.postForLocation(REST_SERVICE_URI+"/app/", app, Appointment.class);
        System.out.println("Location : "+uri.toASCIIString());
    }
 
    /* PUT */
    private static void updateAppointment() throws ParseException {
        System.out.println("Testing update Appointment API----------");
        RestTemplate restTemplate = new RestTemplate();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String dateInString = "12-12-2012";
		Date date = sdf.parse(dateInString);
        Appointment app  = new Appointment(1,"Tomy tomy",date);
        restTemplate.put(REST_SERVICE_URI+"/app/1", app);
        System.out.println(app);
    }
 
    /* DELETE */
    private static void deleteAppointment() {
        System.out.println("Testing delete Appointment API----------");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI+"/app/3");
    }
 
 
    /* DELETE */
    private static void deleteAllAppointments() {
        System.out.println("Testing all delete Appointments API----------");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI+"/app/");
    }
 
    public static void main(String args[]) throws ParseException{
        listAllAppointments();
        getAppointment();
        createAppointment();
        listAllAppointments();
        updateAppointment();
        listAllAppointments();
        deleteAppointment();
        listAllAppointments();
        deleteAllAppointments();
        listAllAppointments();
    }
	
	@Test
	public void contextLoads() {
	}

}
