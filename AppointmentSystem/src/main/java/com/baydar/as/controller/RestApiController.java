package com.baydar.as.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.baydar.as.model.Appointment;
import com.baydar.as.service.AppointmentService;
import com.baydar.as.util.CustomErrorType;

//Rest controller class
//Handles rest transactions
@RestController
@RequestMapping("/api")
public class RestApiController {

	public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

	@Autowired
	AppointmentService appService; // Service which will do all data retrieval/manipulation work

	//Retrieves all appointments
	@RequestMapping(value = "/app/", method = RequestMethod.GET)
	public ResponseEntity<List<Appointment>> listAllAppointments() {
		List<Appointment> apps = appService.findAllAppointments();
		if (apps.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// You many decide to return HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Appointment>>(apps, HttpStatus.OK);
	}

	//Retrieves appointment with given id
	@RequestMapping(value = "/app/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getAppointment(@PathVariable("id") long id) {
		logger.info("Fetching Apps with id {}", id);
		Appointment app = appService.findById(id);
		if (app == null) {
			logger.error("App with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("App with id " + id + " not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Appointment>(app, HttpStatus.OK);
	}

	//Creates an appointment
	@RequestMapping(value = "/app/", method = RequestMethod.POST)
	public ResponseEntity<?> createAppointment(@RequestBody Appointment app, UriComponentsBuilder ucBuilder) {
		logger.info("Creating App : {}", app);
		if (appService.isAppointmentExist(app) || appService.findById(app.getId())!=null) {
			logger.error("Unable to create. A App with name {} already exist", app.getPatient());
			return new ResponseEntity(
					new CustomErrorType("Unable to create. A App with name " + app.getPatient() + " already exist."),
					HttpStatus.CONFLICT);
		}
		appService.saveAppointment(app);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/app/{id}").buildAndExpand(app.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	//Updates an appointment
	@RequestMapping(value = "/app/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateAppointment(@PathVariable("id") long id, @RequestBody Appointment app) {
		logger.info("Updating App with id {}", id);
		Appointment currentApp = appService.findById(id);
		if (currentApp == null) {
			logger.error("Unable to update. App with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to upate. App with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		currentApp.setPatient(app.getPatient());
		currentApp.setDate(app.getDate());
		appService.updateAppointment(currentApp);
		return new ResponseEntity<Appointment>(currentApp, HttpStatus.OK);
	}

	//Deletes an appointment with given id
	@RequestMapping(value = "/app/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteApp(@PathVariable("id") long id) {
		logger.info("Fetching & Deleting App with id {}", id);
		Appointment app = appService.findById(id);
		if (app == null) {
			logger.error("Unable to delete. App with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to delete. App with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		appService.deleteAppointmentById(id);
		return new ResponseEntity<Appointment>(HttpStatus.NO_CONTENT);
	}

	//Deletes all appointments
	@RequestMapping(value = "/app/", method = RequestMethod.DELETE)
	public ResponseEntity<Appointment> deleteAllAppointments() {
		logger.info("Deleting All Appointments");
		appService.deleteAllAppointments();
		return new ResponseEntity<Appointment>(HttpStatus.NO_CONTENT);
	}

}