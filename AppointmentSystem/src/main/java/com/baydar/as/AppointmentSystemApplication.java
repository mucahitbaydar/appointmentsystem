package com.baydar.as;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.baydar.as"})
public class AppointmentSystemApplication {

	public static void main(String[] args) {
		System.setProperty("server.servlet.context-path", "/AppointmentSystem");
		SpringApplication.run(AppointmentSystemApplication.class, args);
	}
}
