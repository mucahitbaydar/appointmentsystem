package com.baydar.as.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;

import com.baydar.as.model.Appointment;

//Appointment Service methods
@Service("appointmentService")
public class AppointmentServiceImpl implements AppointmentService {

	private static final AtomicLong counter = new AtomicLong();
	private static List<Appointment> apps;
	
	//Create some dummy appointments
	static{
		try {
			apps= populateDummyAppointments();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//Returns all appointments
	public List<Appointment> findAllAppointments() {
		return apps;
	}
	
	//Finds appointment by id
	public Appointment findById(long id) {
		for(Appointment app : apps){
			if(app.getId() == id){
				return app;
			}
		}
		return null;
	}
	
	//Find appointment by date
	//For simplicity there could be only 1 appointment per day
	public Appointment findByDate(Date date) {
		for(Appointment app : apps){
			if(app.getDate().compareTo(date)==0){
				return app;
			}
		}
		return null;
	}
	
	//Saves appointment
	public void saveAppointment(Appointment app) {
		app.setId(counter.incrementAndGet());
		apps.add(app);
	}

	//Updates appointment
	public void updateAppointment(Appointment app) {
		int index = apps.indexOf(app);
		apps.set(index, app);
	}

	//Deletes appointment with given id
	public void deleteAppointmentById(long id) {
		for (Iterator<Appointment> iterator = apps.iterator(); iterator.hasNext(); ) {
			Appointment app = iterator.next();
		    if (app.getId() == id) {
		        iterator.remove();
		    }
		}
	}

	//Checks if appointment exists
	public boolean isAppointmentExist(Appointment app) {
		return findByDate(app.getDate())!=null;
	}
	
	//Deletes all appointments
	public void deleteAllAppointments(){
		apps.clear();
	}

	//Creates dummy appointments
	private static List<Appointment> populateDummyAppointments() throws ParseException{
		List<Appointment> apps = new ArrayList<Appointment>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String dateInString = "11-11-2011";
		Date date = sdf.parse(dateInString);
		apps.add(new Appointment(1,"Mx Bx", date));
		dateInString = "12-12-2012";
		date = sdf.parse(dateInString);
		apps.add(new Appointment(2,"Cx Dx", date));
		dateInString = "03-03-2013";
		date = sdf.parse(dateInString);
		apps.add(new Appointment(3,"Fx Gx", date));
		dateInString = "04-04-2014";
		date = sdf.parse(dateInString);
		apps.add(new Appointment(4,"Tx Ux", date));
		counter.set(4);
		return apps;
	}

}
