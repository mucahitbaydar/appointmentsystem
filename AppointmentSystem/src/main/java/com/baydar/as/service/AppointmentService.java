package com.baydar.as.service;

import java.util.Date;
import java.util.List;
import com.baydar.as.model.Appointment;

//Interface for Appointment Service
public interface AppointmentService {
	
	//Method declarations
	Appointment findById(long id);
	Appointment findByDate(Date date);
	void saveAppointment(Appointment app);
	void updateAppointment(Appointment app);
	void deleteAppointmentById(long id);
	List<Appointment> findAllAppointments();
	void deleteAllAppointments();	
	boolean isAppointmentExist(Appointment app);

}
