package com.baydar.as.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Appointment {
	
	private long id;
	private String patient;
	@JsonFormat(pattern="dd-MM-yyyy")
	private Date date;
	
	//Default constructor
	public Appointment() {
		this.id = 0;
	}
	
	//Parameterized constructor
	public Appointment(long id, String patient, Date date) {
		this.id = id;
		this.patient = patient;
		this.date = date;
	}
	
	//Getters and setters
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPatient() {
		return patient;
	}

	public void setPatient(String patient) {
		this.patient = patient;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	@Override
	public String toString() {
		return "Appointment [id=" + id + ", patient=" + patient + ", date=" + date +"]";
	}
	

	
}
